import React from "react";
import { AppStack } from "./navigation";
import { AppLoading } from "expo";
import { useFonts } from "expo-font";

const customFonts = {
  Bold: require("./assets/fonts/Montserrat-Bold.ttf"),
  ExtraBold: require("./assets/fonts/Montserrat-ExtraBold.ttf"),
  Regular: require("./assets/fonts/Montserrat-Regular.ttf"),
  Thin: require("./assets/fonts/Montserrat-Thin.ttf"),
  Lobster: require("./assets/fonts/Lobster-Regular.ttf"),
};
const App = () => {
  const [isLoaded] = useFonts(customFonts);
  if (!isLoaded) {
    return <AppLoading />;
  } else {
    return <AppStack />;
  }
};
export default App;
