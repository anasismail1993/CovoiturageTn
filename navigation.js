import React, { useEffect, useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import {
    createStackNavigator,
    TransitionPresets,
    CardStyleInterpolators
} from "@react-navigation/stack";
import { Image } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import RegisterScreen  from "./screens/Authentication/RegisterScreen";
import LoginScreen  from "./screens/Authentication/LoginScreen";
import GreetingsScreen from './screens/Authentication/GreetingsScreen';
import TabOneScreen from './screens/TabOneScreen'
import OffresScreen from './screens/Tabs/OffresScreen';
import DemandesScreen from './screens/Tabs/DemandesScreen';
import ProfileScreen from './screens/Tabs/ProfileScreen';
import GuidScreen from './screens/Intro/GuidScreen';
import OnBoardScreen from './screens/Intro/OnBoardScreen';


const Tab = createBottomTabNavigator();

function MainTabs() {
    return (
        <Tab.Navigator
            tabBarOptions={{
                showIcon: true,
                activeTintColor: "#3B5998",
                inactiveTintColor: "gray"
            }}
        >
            <Tab.Screen
                     options={{
                        tabBarLabel: "Offres",
                        tabBarIcon: ({ focused }) => (
                          <Image
                            style={{  resizeMode: "contain" }}
                            source={
                              focused
                                ? require("./assets/icons/OffresSelected.png")
                                : require("./assets/icons/Offres.png")
                            }
                          />
                        )
                      }}
                name="Offres"
                component={OffresScreen}
            />
            <Tab.Screen
             options={{
                tabBarLabel: "Demandes",
                tabBarIcon: ({ focused }) => (
                  <Image
                    style={{  resizeMode: "contain" }}
                    source={
                      focused
                        ? require("./assets/icons/DemandesSelected.png")
                        : require("./assets/icons/Demandes.png")
                    }
                  />
                )
              }}
                name="Demandes"
                component={DemandesScreen}
            />
            <Tab.Screen
               options={{
                tabBarLabel: "Profile",
                tabBarIcon: ({ focused }) => (
                  <Image
                    style={{  resizeMode: "contain" }}
                    source={
                      focused
                        ? require("./assets/icons/UserSelected.png")
                        : require("./assets/icons/User.png")
                    }
                  />
                )
              }}
                name="Profile"
                component={ProfileScreen}
            />
        </Tab.Navigator>
    );
}

const Stack = createStackNavigator();

export function AppStack() {
    return (
            <NavigationContainer >
                <Stack.Navigator
                    screenOptions={{
                        headerShown: false,
                    }}
                >
                            <Stack.Screen name="Guid" component={GuidScreen} />
                            <Stack.Screen name="OnBoard" component={OnBoardScreen} />
                            <Stack.Screen name="Tabs" component={MainTabs} />
                            <Stack.Screen name="Register" component={RegisterScreen} />
                            <Stack.Screen name="Login" component={LoginScreen} />
                            <Stack.Screen name="Greetings" component={GreetingsScreen} />
                            <Stack.Screen name="TabOne" component={TabOneScreen} />
                </Stack.Navigator>
            </NavigationContainer>
    );
}
