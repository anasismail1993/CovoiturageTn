import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  SafeAreaView,
  ScrollView,
  FlatList,
  View,
  Image,
} from "react-native";
import {
  Card,
  CardTitle,
  CardContent,
  CardAction,
  CardButton,
  CardImage,
} from "react-native-material-cards";
import { Icon, Avatar, Button } from "react-native-elements";
import { Text } from "galio-framework";



const DATA = [
  {
    avatar: 'https://imganuncios.mitula.net/renault_clio_2007_essence_belle_clio_classique_8090135592504549776.jpg',
    title: "User 1",
    dist: "vers tunis",
    date: "18/1/2013",
  },
  {
    avatar: "https://www.binbincar.tn/Content/uploads/201902/image20190222_193502_635565.jpg",
    title: "User 2",
    dist: "vers sousse",
    date: "20/2/2020",
  },
  {
    avatar: "https://www.binbincar.tn/Content/uploads/201809/image20180910_160502_745856.jpg",
    title: "User 3",
    dist: "vers Tunis",
    date: "01/01/2020",
  },
];


export default function OffresScreen({ navigation }) {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView>
            {/* Titre Destinations*/}
            <Text
          h6
          style={{
            fontFamily: "Bold",
            marginTop: 10,
            marginBottom: 10,
            marginLeft: 20,
          }}
        >
          Destinations populaire
        </Text>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <View style={{ width: "50%" }}>
            <Card
              mediaSource={{
                uri:
                  "https://lp-cms-production.imgix.net/2019-06/51f617c9f5219dded075f760e85919f4-sousse.jpg",
              }}
            >
              <CardContent
                text="Tunis vers Sousse "
                textStyle={{ fontFamily: "Bold", marginTop: 90 }}
              />
            </Card>
          </View>
          <View style={{ width: "50%" }}>
            <Card
              mediaSource={{
                uri:
                  "https://res.cloudinary.com/dprtxlyhi/image/upload/21076_4158_zphrmy.jpg",
              }}
            >
              <CardContent
                text="Sousse Vers Tunis "
                textStyle={{ fontFamily: "Bold", marginTop: 90 }}
              />
            </Card>
          </View>
        </View>
        
        <View style={{ flex: 1 }}>
          <View
            style={{
              flex: 2,
              flexDirection: "row",
              backgroundColor: "#F9F9F9",
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20,
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
            }}
          >
            <View
              style={{
                height: "100%",
                width: "60%",
                flexDirection: "column",
                justifyContent: "center",
              }}
            >
              <Text h6 style={{ fontFamily: "Lobster", marginLeft: 20 }}>
                où tu veux aller
              </Text>
            </View>
          </View>
          {/* Titre Partie */}
          <View style={{flexDirection:'row',justifyContent:'space-between'}}>
          <Text
            h6
            style={{
              fontFamily: "Bold",
              marginTop: 10,
              marginBottom: 10,
              marginLeft: 20,
            }}
          >
            à jour
          </Text>
          <Button
  title="Voir tout "
type="clear"
style={{ marginTop: 10,marginRight:20}}
/>
          </View>
          
          {/* Card 1*/}
          <Card
            mediaSource={{
              uri:
                "https://popiah.files.wordpress.com/2010/03/mini-cooper-d.jpg",
                height:"66%"
            }}
            style={{backgroundColor:'white'}}
          >
            <CardContent
              avatarSource={{
                uri:
                  "https://scontent.ftun11-1.fna.fbcdn.net/v/t1.0-9/119261363_2672995086255690_8706759257484110969_o.jpg?_nc_cat=106&_nc_sid=09cbfe&_nc_ohc=EC56JPeEFbQAX8a2VEx&_nc_ht=scontent.ftun11-1.fna&oh=7e4552455e5027e05ac8858897f92d7e&oe=5F8FA07C",
              }}
              text="covoiturage disponible de sousse vers tunis ! tous infos dispo dans l'annonce! merci bcp "
              textStyle={{ fontFamily: "Regular" }}
            />
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-evenly",
              }}
            >
              <View>
                <Icon name="place" color="white" style={{ marginTop: 10 }} />
                <Text style={{ fontFamily: "Regular", color: "white" }}>
                  Sousse
                </Text>
              </View>
              <View>
                <Icon
                  name="directions"
                  color="white"
                  style={{ marginTop: 10 }}
                />
                <Text style={{ fontFamily: "Regular", color: "white" }}>
                  Tunis
                </Text>
              </View>
            </View>

            <View
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "space-between",
                backgroundColor: "white",
                opacity: 0.7,
                width: "100%",
                borderBottomLeftRadius: 20,
                borderBottomRightRadius: 20,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}
            >
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  marginLeft: 20,
                }}
              >
                <Icon name="today" style={{ marginTop: 10 }} />
                <Text style={{ fontFamily: "Regular", color: "black" }}>
                  18/04/1993
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "black",
                  width: "0.2%",
                  height: "50%",
                  alignSelf: "center",
                }}
              ></View>
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Icon name="alarm" />
                <Text style={{ fontFamily: "Regular", color: "black" }}>
                  15:00
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "black",
                  width: "0.2%",
                  height: "50%",
                  alignSelf: "center",
                }}
              ></View>
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  marginRight: 20,
                }}
              >
                <Icon name="local-car-wash" />
                <Text style={{ fontFamily: "Regular", color: "black" }}>
                  Toyota
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "black",
                  width: "0.2%",
                  height: "50%",
                  alignSelf: "center",
                }}
              ></View>
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  marginRight: 20,
                }}
              >
                <Icon name="accessibility" />
                <Text style={{ fontFamily: "Regular", color: "black" }}>
                  3 places
                </Text>
              </View>
            </View>

            <CardAction separator={true} inColumn={false}>
              <CardButton
                onPress={() => {}}
                title="details"
                color="white"
                titleStyle={{ fontFamily: "Bold" }}
              />
              <CardButton
                onPress={() => {}}
                title="Reserver"
                color="white"
                titleStyle={{ fontFamily: "Bold" }}
              />
            </CardAction>
          </Card>
        </View>

        {/* Card 2*/}
        <View style={{ flex: 1 }}>
          <Card
            mediaSource={{
              uri:
                "https://cdn.proxyparts.com/vehicles/100457/3669149/large/991d19b3-2730-4703-80f6-c7dcb0a22ed7.jpg",
                height:"60%"
            }}
          >
            <CardContent
              avatarSource={{
                uri:
                  "https://scontent.ftun11-1.fna.fbcdn.net/v/t31.0-8/14712940_747826218689857_989607954567659265_o.jpg?_nc_cat=101&_nc_sid=09cbfe&_nc_ohc=dMuR9g1x0EUAX8A6LBf&_nc_oc=AQm0BMOZrJn56FOtb6NXwci40Q1pCR3iReDGNYZjP6lEiYFVZddFYnfCwMprekxCJJ4&_nc_ht=scontent.ftun11-1.fna&oh=9a1ca2939dbe2adb24aa2123252d352d&oe=5F9047BC",
              }}
              text="covoiturage disponible de sousse vers tunis ! tous infos dispo dans l'annonce! merci bcp "
              textStyle={{ fontFamily: "Regular" }}
            />
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-evenly",
              }}
            >
              <View>
                <Icon name="place" color="white" style={{ marginTop: 10 }} />
                <Text style={{ fontFamily: "Regular", color: "white" }}>
                  Sousse
                </Text>
              </View>
              <View>
                <Icon
                  name="directions"
                  color="white"
                  style={{ marginTop: 10 }}
                />
                <Text style={{ fontFamily: "Regular", color: "white" }}>
                  Tunis
                </Text>
              </View>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "space-between",
                backgroundColor: "white",
                opacity: 0.7,
                width: "100%",
                borderBottomLeftRadius: 20,
                borderBottomRightRadius: 20,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
              }}
            >
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  marginLeft: 20,
                }}
              >
                <Icon name="today" style={{ marginTop: 10 }} />
                <Text style={{ fontFamily: "Regular", color: "black" }}>
                  18/04/1993
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "black",
                  width: "0.2%",
                  height: "50%",
                  alignSelf: "center",
                }}
              ></View>
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Icon name="alarm" />
                <Text style={{ fontFamily: "Regular", color: "black" }}>
                  15:00
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "black",
                  width: "0.2%",
                  height: "50%",
                  alignSelf: "center",
                }}
              ></View>
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  marginRight: 20,
                }}
              >
                <Icon name="local-car-wash" />
                <Text style={{ fontFamily: "Regular", color: "black" }}>
                  Toyota
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: "black",
                  width: "0.2%",
                  height: "50%",
                  alignSelf: "center",
                }}
              ></View>
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  marginRight: 20,
                }}
              >
                <Icon name="accessibility" />
                <Text style={{ fontFamily: "Regular", color: "black" }}>
                  3 places
                </Text>
              </View>
            </View>

            <CardAction separator={true} inColumn={false}>
              <CardButton
                onPress={() => {}}
                title="details"
                color="white"
                titleStyle={{ fontFamily: "Bold" }}
              />
              <CardButton
                onPress={() => {}}
                title="Reserver"
                color="white"
                titleStyle={{ fontFamily: "Bold" }}
              />
            </CardAction>
          </Card>
        </View>

    <Text
          h6
          style={{
            fontFamily: "Bold",
            marginTop: 10,
            marginBottom: 10,
            marginLeft: 20,
          }}
        >
          Earlier today
        </Text>

        <FlatList
          data={DATA}
          horizontal
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          renderItem={({ item }) => (
            <Card>
              <CardImage
                source={item.avatar}
                title={item.title}
              />
              <CardTitle 
    title="This is a title" 
    subtitle="This is subtitle"
   />
            </Card>
          )}
          keyExtractor={(item) => item.id}
        />

      </ScrollView>
    </SafeAreaView>
  );
}
