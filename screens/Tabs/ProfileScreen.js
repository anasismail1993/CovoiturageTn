import React, { useState, useEffect } from "react";
import { View, KeyboardAvoidingView, FlatList } from "react-native";
import { Text } from "galio-framework";
import { Card } from "@paraboly/react-native-card";
import { Avatar, Icon, Button } from "react-native-elements";


const DATA = [
  {
    desc: "parmis les meilleurs ! recommended !",
    title: "User 1",
    dist:"120 km",
    date:'18/1/2013'
  },
  {
    desc: "bcp de respect et bon ambiance ",
    title: "User 2",
    dist:"100 km",
    date:"20/2/2020"
  },
  {
    desc: "c'etait tres bien et sympa ",
    title: "User 3",
    dist:"80 km",
    date:"01/01/2020"
  },
];



export default function ProfileScreen({ navigation }) {
  return (
    <View style={{ flex: 1, backgroundColor: "white" }}>
      {/* partie header avec avatar */}
      <View
        style={{
          flex: 2,
          flexDirection: "row",
          backgroundColor: "#F9F9F9",
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}
      >
        <View
          style={{
            height: "100%",
            width: "60%",
            flexDirection: "column",
            justifyContent: "center",
          }}
        >
          <Text h6 style={{ fontFamily: "Lobster", marginLeft: 20 }}>
            Anas Ismail
          </Text>
          <Text style={{ fontFamily: "Regular", marginLeft: 20 }}>
            débutant
          </Text>
        </View>
        <View
          style={{
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            height: "100%",
            width: "40%",
          }}
        >
          <Avatar
            rounded
            size="large"
            source={{
              uri:
                "https://scontent.ftun11-1.fna.fbcdn.net/v/t1.0-9/119261363_2672995086255690_8706759257484110969_o.jpg?_nc_cat=106&_nc_sid=09cbfe&_nc_ohc=EC56JPeEFbQAX8a2VEx&_nc_ht=scontent.ftun11-1.fna&oh=7e4552455e5027e05ac8858897f92d7e&oe=5F8FA07C",
            }}
          ></Avatar>
          <Button title="Editer" type="clear" />
        </View>
      </View>

      {/* partie informations perso */}
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "space-between",
          backgroundColor: "#395AFF",
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}
      >
        <View
          style={{
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            marginLeft: 20,
          }}
        >
          <Text style={{ fontFamily: "Regular", color: "white" }}> Nom </Text>
          <Text
            style={{ fontFamily: "Bold", color: "white" }}
          >
            Anas Ismail
          </Text>
        </View>
        <View
          style={{
            backgroundColor: "black",
            width: "0.2%",
            height: "50%",
            alignSelf: "center",
          }}
        ></View>
        <View
          style={{
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text style={{ fontFamily: "Regular", color: "white" }}> Age </Text>
          <Text
            style={{ fontFamily: "Bold",  color: "white" }}
          >
            26
          </Text>
        </View>
        <View
          style={{
            backgroundColor: "black",
            width: "0.2%",
            height: "50%",
            alignSelf: "center",
          }}
        ></View>
        <View
          style={{
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            marginRight: 20,
          }}
        >
          <Text style={{ fontFamily: "Regular", color: "white" }}>emploi</Text>
          <Text
            style={{ fontFamily: "Bold", color: "white" }}
          >
            Etudiant
          </Text>
        </View>
        <View
          style={{
            backgroundColor: "black",
            width: "0.2%",
            height: "50%",
            alignSelf: "center",
          }}
        ></View>
        <View
          style={{
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            marginRight: 20,
          }}
        >
          <Text style={{ fontFamily: "Regular", color: "white" }}>num tél</Text>
          <Text
            style={{ fontFamily: "Bold", color: "white" }}
          >
            25915556
          </Text>
        </View>
      </View>

      {/* Partie info profile  */}
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "space-between",
          backgroundColor: "#F9F9F9",
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
        }}
      >
        <View
          style={{
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            marginLeft: 20,
          }}
        >
          <Icon name="today" />
          <Text style={{ fontFamily: "Regular" }}> depuis 2019 </Text>
        </View>
        <View
          style={{
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Icon name="navigation" />
          <Text style={{ fontFamily: "Regular" }}> 3 Voyages</Text>
        </View>
        <View
          style={{
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            marginRight: 20,
          }}
        >
          <Icon name="star" />
          <Text style={{ fontFamily: "Regular" }}> 4,5 of 5 </Text>
        </View>
      </View>

      {/* partie Recommendations */}
      <View>
      <Text style={{ fontFamily: "Bold",marginTop:10,marginBottom:10 }}> Recommendations</Text>
      <FlatList
                      data={DATA}
                      horizontal
                      showsHorizontalScrollIndicator={false}
                      showsVerticalScrollIndicator={false}
                      renderItem={({ item }) => (
                        <Card
                        title={item.title}
                        iconName="thumbs-up"
                        defaultTitle=""
                        iconType="Entypo"
                        defaultContent=""
                        onPress={() => {}}
                        topRightText={item.date}
                        bottomRightText={item.dist}
                        content={item.desc}
                      />
                      )}
                      keyExtractor={(item) => item.id}
                    />
      </View>
        {/* Partie Voyages precedents  */}
      <View
        style={{
          flex: 4,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <View>
          <Icon name="history" />
          <Text style={{ fontFamily: "Regular" }}>
            your previous trips here
          </Text>
        </View>
      </View>
    
  

     
    </View>
  );
}
