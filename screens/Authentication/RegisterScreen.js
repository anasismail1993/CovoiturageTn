import React, { useState, useEffect } from "react";
import { View, KeyboardAvoidingView,Image } from "react-native";
import { Text, Input } from "galio-framework";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function RegisterScreen({ navigation }) {
  return (
    <View style={{ flex: 1,backgroundColor:'white'}}>
        <View style={{ flex:1,justifyContent:'flex-start'}}>
            <View style={{width:'20%'}}>
            <Button
            type="clear"
  icon={
    <Icon
      name="arrow-left"
      size={15}
      color="#395AFF"
    />
  }
  title=" back"
  onPress={() => {
    navigation.navigate("OnBoard");
  }}
/>
            </View>
       
        </View>
      <View
        style={{
          flex: 9,
          alignItems: "center",
          justifyContent: "center",
        
        }}
      >
        <KeyboardAvoidingView>
        <View style={{ alignItems:'center', justifyContent:'center',flexDirection:'row'}}>
        <Text h4 style={{ fontFamily: 'Bold' }}> Covoiturage </Text>
        <Text h4 style={{ fontFamily: 'Bold',color:'#395AFF' }}>TN </Text>
        <Image source={require("../../assets/icons/Covoiturage.png")} />
        </View>
        <View style={{alignItems:'center', justifyContent:'center',marginBottom:20}}>
          <Text style={{fontFamily:'Thin',fontSize: 14,}}> vous devez d'abord vous connecter</Text>
        </View>
        <Input placeholder="Nom et Prenom" rounded />
        <Input rounded type="email-address" placeholder="Email" autoCapitalize="none" />
        <Input rounded password placeholder="Password" viewPass  autoCapitalize="none" />
        <Input placeholder="Tel" type="number-pad" rounded />
        <Input placeholder="age" type="number-pad" rounded />
        <View style={{alignItems:'center',marginTop:20,flexDirection:'row',justifyContent:'space-around'}}>
        <Button title="Signup" type="outline" />
        </View>
        
        </KeyboardAvoidingView>
      </View>
      
    </View>
  );
}

