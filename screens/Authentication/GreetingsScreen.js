import React, { useState, useEffect } from "react";
import { View, KeyboardAvoidingView,Image,StyleSheet,StatusBar } from "react-native";
import {
    Block, Icon, Text, NavBar,Button
  } from 'galio-framework';
import { LinearGradient } from 'expo-linear-gradient'
import Constants from 'expo-constants';
import theme from '../../theme';


export default function GreetingsScreen({ navigation }) {
  return (
<Block flex>
    <StatusBar hidden={false} barStyle="light-content" />
    <Image
      style={styles.backgroundImage}
      source={{ uri: 'https://images.unsplash.com/photo-1600695687503-a8ec19e706c7?ixlib=rb-1.2.1&auto=format&fit=crop&w=900&q=80' }}
    />
    <Block flex space="between" center style={styles.absolute}>
      <NavBar transparent leftIconColor={theme.COLORS.WHITE} onLeftPress={() => props.navigation.openDrawer()} />
      <Block style={styles.articleSummary}>
        <Block row style={{ marginBottom: theme.SIZES.BASE }}>
          <Block row middle style={{ marginRight: theme.SIZES.BASE }}>
            <Text
              h6
              style={{fontFamily: 'Bold'}}
            >
            Bienvenue
            </Text>
          </Block>
          <Block row middle>
            <Text
            h2
            style={{fontFamily: 'Lobster',color:'#395AFF'}}
            >Anas
            </Text>
          </Block>
        </Block>

        <Block>
          <Text
            style={{fontFamily: 'Regular'}}
          >
           nous vous recommandons fortement de mettre à jour votre profil
          </Text>
          <Button color="info" style={{alignSelf:'center',marginTop:20}} onPress={() => { navigation.navigate("Tabs")}}>Continuer</Button>
        </Block>
        <LinearGradient colors={['transparent', 'rgba(0, 0, 0, 0.7)']} style={styles.gradient} />
      </Block>
    </Block>
  </Block>
    );
}
const styles = StyleSheet.create({
    backgroundImage: {
      flex: 1,
      width: '100%',
      resizeMode:'cover'
    },
    absolute: {
      position: 'absolute',
      bottom: 0,
      top: Constants.statusBarHeight,
      left: 0,
      right: 0,
    },
    gradient: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      height: 90,
    },
    articleSummary: {
      paddingLeft: 20,
      paddingBottom: 20,
      paddingRight: '10%',
    },
  });
  

