import React, { useState, useEffect } from "react";
import { View,Image } from "react-native";
import { Button, Text } from "galio-framework";

export default function OnBoardScreen({ navigation }) {
  return (
    <View style={{ flex: 1 ,backgroundColor:'white'}}>
      <View style={{ flex: 3 ,alignItems:'center', justifyContent:'center'}}>
        <View style={{ alignItems:'center', justifyContent:'center',flexDirection:'row'}}>
        <Text h4 style={{ fontFamily: 'Bold' }}> Covoiturage </Text>
        <Text h4 style={{ fontFamily: 'Bold',color:'#395AFF' }}>TN </Text>
        <Image source={require("../../assets/icons/Covoiturage.png")} />
        </View>
        <View>
          <Text style={{fontFamily:'Thin',fontSize: 14,}}> Voyagez avec des gens. Se faire de nouveaux amis.</Text>
        </View>
        
      </View>
      <View style={{ flex: 1, alignItems: "center" }}>
        <Button round color="#395AFF" onPress={() => {
                navigation.navigate("Register");
              }}>
          Sign up
        </Button>
        <Button round color="#395AFF" onPress={() => {
                navigation.navigate("Login");
              }}>
          Log in
        </Button>
      </View>
    </View>
  );
}
