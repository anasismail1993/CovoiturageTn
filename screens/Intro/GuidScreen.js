import React, { useState, useEffect } from "react";
import { View } from "react-native";
import { Button } from "react-native-elements";
import { Text } from "galio-framework";
import Onboarding from "react-native-onboarding-swiper";
import { Image, elevation } from "react-native";
import NextButton from "react-native-onboarding-swiper/src/buttons/NextButton";
export default function GuidScreen({ navigation }) {
  return (
    <Onboarding
      onDone={() => navigation.navigate("Tabs")}
      onSkip={() => navigation.navigate("Tabs")}
      showNext={false}
      showDone={false}
      bottomBarColor={"white"}
      showSkip={false}
      pages={[
        {
          backgroundColor: "white",
          image: (
            <Image
              source={require("../../assets/images/Guid/car.jpg")}
              style={{ width: 390, height: 400 , borderBottomLeftRadius:20,borderBottomRightRadius:20,borderTopLeftRadius:20,borderTopRightRadius:20 }}
            />
          ),
          title: (
            <View style={{alignItems:'center',marginBottom:20}}>
              <Text
                style={{
                  fontSize: 22,
                  fontFamily: 'Bold'
                }}
              >
                Offre Votre Service
              </Text>
              <Text
                style={{
                  fontSize: 20,
                  fontFamily: 'Regular',
                  color: "#9F9F9F",
                }}
              >
                Partager Le Coût
              </Text>
            </View>
          ),
          subtitle: (
            <Button
              title="Done"
              type="outline"
              raised={true}
              loading={true}
              onPress={() => {
                navigation.navigate("Tabs");
              }}
            >
              next
            </Button>
          ),
        },
        {
          backgroundColor: "white",
          image: (
            <Image
              source={require("../../assets/images/Guid/friends.jpg")}
              style={{ width: 390, height: 400,borderBottomLeftRadius:20,borderBottomRightRadius:20,borderTopLeftRadius:20,borderTopRightRadius:20 }}
            />
          ),
          title: (
            <View style={{alignItems:'center',marginBottom:20}}>
            <Text
              style={{
                fontSize: 21,
                fontFamily: 'Bold'
              }}
            >
              Rencontrer Des Nouveaux Amis
            </Text>
            <Text
              style={{
                fontSize: 21,
                fontFamily: 'Regular',
                color: "#9F9F9F",
              }}
            >
              De Tous Les Regions
            </Text>
          </View>
          ),
          subtitle: (
            <Button
              title="Done"
              type="outline"
              raised={true}
              loading={true}
              style={{}}
              onPress={() => {
                navigation.navigate("Tabs");
              }}
            >
              next
            </Button>
          ),
        },
        {
          backgroundColor: "white",
          image: (
            <Image
              source={require("../../assets/images/Guid/late.jpg")}
              style={{ width: 390, height: 400 , borderBottomLeftRadius:20,borderBottomRightRadius:20,borderTopLeftRadius:20,borderTopRightRadius:20}}
            />
          ),
          title: (
            <View style={{alignItems:'center',marginBottom:20}}>
            <Text
              style={{
                fontSize: 22,
                fontFamily: 'Bold'
              }}
            >
              Pas De Retard 
            </Text>
            <Text
              style={{
                fontSize: 20,
                fontFamily: 'Bold',
                color: "#9F9F9F",
              }}
            >
planifier à l'avance
            </Text>
          </View>
          ),
          subtitle: (
            <Button
              title="Done"
              raised={true}
              type="outline"
              onPress={() => {
                navigation.navigate("OnBoard");
              }}
            >
              next
            </Button>
          ),
        },
      ]}
    />
  );
}
